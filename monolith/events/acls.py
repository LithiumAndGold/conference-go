import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    query = {"query": f"{city}, {state}"}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=query, headers=headers)
    content = response.json()
    try:
        picture_url = content["photos"][0]["src"]["original"]
        return {"picture_url": picture_url}
    except (KeyError, IndexError):
        return {"picture_url": None}

    # This is how you test it
    # get_photo("New York", "New York")

    # This is what you put into insomnia
    # https://api.pexels.com/v1/search?query=newyork&query=newyork


def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {"q": f"{city}, {state}, US", "appid": OPEN_WEATHER_API_KEY}
    response = requests.get(url, params=params)
    content = response.json()
    lat = content[0]["lat"]
    lon = content[0]["lon"]
    # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude

    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    #   and longitude
    # Make the request
    response2 = requests.get(url)
    # Parse the JSON response
    content2 = response2.json()
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    temp = content2["main"]["temp"]
    description = content2["weather"][0]["description"]
    dict = {
        "temp": temp,
        "description": description
    }
    # Return the dictionary
    return dict
